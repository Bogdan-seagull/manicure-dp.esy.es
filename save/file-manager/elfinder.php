$ nano elfinder/elfinder.php[/stextbox]
<? require_once("login/login.php"); ?>
<!DOCTYPE html>
<html>
	<head>

		<meta charset="utf-8">

		<title>Файловый менеджер</title>

		<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="../css/elfinder.css">

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>

	</head>
	<body>

		<? if(!$auth->isAuth()){ ?>

		<div class="login_form">
			<form method="POST" action="">
				<div class="head-form">
					<h1>Файловый менеджер</h1>
				</div>
				<div class="input-form">
					<input class="validate" type="text" name="login" placeholder="Login" value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; ?>">
				</div>
				<div class="input-form">
					<input class="validate" type="password" name="password" placeholder="Password">
				</div>
				<div class="btn-form">
					<input class="submit" type="submit" value="Login">
				</div>
			</form>
		</div>

		<? } else {

			echo $_SERVER['PHP_SELF'];
?>

		<script src="js/elfinder.js"></script>
		<script src="js/i18n/elfinder.ru.js"></script>
<!-- elFinder initialization (REQUIRED) -->
		<script type="text/javascript" charset="utf-8">
			$().ready(function() {
				var elf = $('#elfinder').elfinder({
					url : 'php/connector.php-dist',
					lang: 'ru'
				}).elfinder('instance');
			});
		</script>
<!-- Element where elFinder will be created (REQUIRED) -->
		<div id="elfinder"></div>
		<p><a href='?is_exit=1'>Logout</a></p>

<?php } ?>

	</body>
</html>
