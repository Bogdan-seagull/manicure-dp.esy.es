<?php
$site_name = "Маникюр в Приднепровске";
$site_url = "http://".$_SERVER['SERVER_NAME'];
$description = "Маникюр в Приднепровске: Красиво выглядящие ухоженные руки – это одно из главных достоинств современной стильной женщины.
								Посмотрев на маникюр, можно многое понять про его обладательницу и оценить уровень ее успешности, вкуса и настроения.
								Ухоженные руки – визитная карточка как женщины, так и мужчины.
								Кому же не хочется иметь руки с нежной ухоженной кожей и прекрасным маникюром?";
$description_img = $site_url."/img/page_media.jpg";
$alt_img = $site_name." ".$page_title;
$schema = "
<div itemscope itemtype='http://schema.org/LocalBusiness'>
	<a itemprop='url' href='$site_url'>
		<div itemprop='name'>
			<strong>$site_name</strong>
		</div>
	</a>
	<div itemprop='description'>$description</div>
	<div itemprop='address' itemscope itemtype='http://schema.org/PostalAddress'>
		<span itemprop='streetAddress'>Дніпропетровськ, ул. Немировича-Данченка 44, кв. 4,</span><br>
		<span itemprop='addressLocality'>Днепропетровск </span><br>
		<span itemprop='addressRegion'>Днепропетровская</span><br>
		<span itemprop='postalCode'>49127</span><br>
		<span itemprop='addressCountry'>Украина </span><br>
	</div>
	Phone:
	<span itemprop='telephone'>+380639653292</span>
	<span itemprop='telephone'>+380678444354</span>
	<span itemprop='telephone'>+380994028399</span>
</div>
<div itemscope itemtype='http://schema.org/GeoCoordinates'>
 <span itemprop='latitude'> 48.4157055 N </span>
 <span itemprop='longitude'> 35.1223603 W </span>
</div>
"
?>
<!DOCTYPE html>
<html lang="ru" hreflang="uk">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9" hreflang="uk">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="title" content="<? echo $alt_img; ?>">
		<meta name="description" content="<? echo $description.$description_img ?>">

		<? echo "<title>".$page_title."</title>"; ?>

		<meta property="og:locale" content="ru_RU">
		<meta property="og:type" content="article">
		<meta property="og:description" content="<? echo $description ?>">
		<meta property="og:title" content="<? echo $alt_img; ?>">
		<meta property="og:url" content="<? echo $site_url; ?>">
		<meta property="og:image" content="<? echo $description_img ?>">
		<meta property="cookbook:food:url" content="<? echo $description_img ?>">
		<meta property="og:site_name" content="<? echo $alt_img; ?>">

		<link rel="shortcut icon" href="<? echo $site_url; ?>/img/favicon.ico"  type="image/x-icon">
		<link rel="stylesheet" href="<? echo $site_url; ?>/css/style.css">
		<link rel="stylesheet" href="<? echo $site_url; ?>/css/animated.css">

		<script type="text/javascript" src="http://code.jquery.com/jquery-2.2.0.min.js"></script>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-62674935-4', 'auto');
			ga('send', 'pageview');

		</script>

		<? echo "<div class='schema'>".$schema."</div>"; ?>

	</head>
	<body>
