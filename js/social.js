
	function newMyWindow1(href) {
		var d = document.documentElement,
			 h = 500,
			 w = 500,
			 myWindow = window.open(href, 'myWindow', 'scrollbars,height=' + Math.min(h, screen.availHeight) + ',width=' + Math.min(w, screen.availWidth) + ',left=' + Math.max(0, ((d.clientWidth - w) / 2 + window.screenX)) + ',top=' + Math.max(0, ((d.clientHeight - h)/1.25 + window.screenY)));

		// абзац для Chrome
		if (myWindow.screenY >= (screen.availHeight - myWindow.outerHeight)) {
			myWindow.moveTo(myWindow.screenX, (screen.availHeight - myWindow.outerHeight));
		}
		if (myWindow.screenX >= (screen.availWidth - myWindow.outerWidth)) {
			myWindow.moveTo((screen.availWidth - myWindow.outerWidth), myWindow.screenY);
		}

	}
