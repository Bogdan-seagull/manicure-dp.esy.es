(function($) {
  $(function() {

  	var supereffectLeft = "fadeInLeft";
  	var itemclassLeft = ".column_cell-left";

    var supereffectRight = "fadeInRight";
    var itemclassRight = ".column_cell-right";

    $(itemclassLeft).find("div").css({opacity: 0});
  	$(itemclassRight).find("div").css({opacity: 0});

  	$(document).on("scroll", function() {
  	  $(itemclassLeft).each(function() {
  	    if ($(this).position().top + $(this).height() <= $(document).scrollTop() + $(window).height()) {
  	      $(this).find("div").animate({opacity: 1}, 4000);
  	      $(this).find("div").addClass(supereffectLeft + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $(this).find("div").removeClass();
          });
  	    }
  	  });
    });

    $(document).on("scroll", function() {
      $(itemclassRight).each(function() {
  	    if ($(this).position().top + $(this).height() <= $(document).scrollTop() + $(window).height()) {
  	      $(this).find("div").animate({opacity: 1}, 4000);
  	      $(this).find("div").addClass(supereffectRight + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $(this).find("div").removeClass();
          });
  	    }
  	  });
  	});

  });
})(jQuery)
