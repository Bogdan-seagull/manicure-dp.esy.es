$(document).ready(function(){

  var header = $("#header");
  var screen = $(window).height() + header.outerHeight();

  $('.btn_toggle_cell').click(function(event){
    $(this).next('.catalog_cell').toggle();
    $('.icon-plus', this).toggleClass('icon-minus');
    console.log();
    return false;
  });

  $(window).scroll(function(){

    var scroll = $(this).scrollTop();
    header_minimize(header, screen, scroll);

  });

});

function header_minimize (header, screen, scroll){
  if( scroll >= screen ){
    header.css({'padding':'0.5em 1em'});
  } else if( scroll < screen + '200'){
    header.css({'padding':'1.5em'});
  }
}
