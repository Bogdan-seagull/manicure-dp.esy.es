<?php
 session_start(); //Запуск сессии

/* Класс авторизации */
class AuthClass {

	private $_login = "Yra"; //Логин
	private $_password = "15011978"; // Пароль

	/**
	* Проверяет, авторизован пользователь или нет
	* Возвращает true если авторизован
	*/
	public function isAuth(){
		if(isset($_SESSION["is_auth"])){ //Если сессия существует
			return $_SESSION["is_auth"]; //Возвращаем значение
		}
		else return false;
	}
	/* Авторизация пользователя */
	public function auth($login, $passwors){
		if ($login == $this->_login && $passwors == $this->_password){ //Если авторизация прошла валидацию то
			$_SESSION["is_auth"] = true; //Делаем пользователя авторизированным
			$_SESSION["login"] = $login; //Записываем логин в сессию
			return true;
		} else { //Логин и пароль не подошел
			$_SESSION["is_auth"] = false;
			return false;
		}
	}
	/* Возвращение логина авторизированного пользователя */
	public function getLogin(){
		if($this->isAuth()){ //Коль пользователь авторизирован
			return $_SESSION["login"]; //Возвращаем логин из сессии
		}
	}
	/* Уничтожение сессии */
	public function out(){
		$_SESSION = array(); //Очищяем сессию
		session_destroy(); //Уничтожаем
	}
}

$auth = new AuthClass();

if(isset($_POST["login"]) && isset($_POST["password"])){ //Если логин и пароль были отправлены
	if(!$auth->auth($_POST["login"], $_POST["password"])){ //Если логи и пароль неправильно введены
		echo "<p>Логин и пароль неверны!</p>";
	}
}

if(isset($_GET["is_exit"])){ //Если нажат выход
	if($_GET["is_exit"] == 1){
		$auth->out(); //Выходи
		header("Location: ?is_exit=0");
	}
}

?>
