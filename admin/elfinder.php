<? require_once("login/login.php"); ?>
<!DOCTYPE html>
<html>
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Файловый менеджер</title>

		<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="../css/elfinder.css">

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.js"></script>

	</head>
	<body>

		<? if(!$auth->isAuth()){ ?>

		<div class="login_form">
			<form method="POST" action="#">
				<div class="head-form">
					<h1>Файловый менеджер</h1>
				</div>
				<div class="input-form">
					<input class="validate" type="text" name="login" placeholder="Login" value="<?php echo (isset($_POST["login"])) ? $_POST["login"] : null; ?>">
				</div>
				<div class="input-form">
					<input class="validate" type="password" name="password" placeholder="Password">
				</div>
				<div class="btn-form">
					<input class="submit" type="submit" value="Login">
				</div>
			</form>
		</div>

		<? } else { ?>

			<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
			<script src="js/elfinder.min.js"></script>
			<script src="js/i18n/elfinder.ru.js"></script>
			<script>
				$(document).ready(function() {
					$('#elfinder').elfinder({
						url : 'php/connector.minimal.php',
						height: $(window).height() - 2,
						lang: 'ru'
					});
				//	$('.ui-helper-clearfix ').append('<a href="?is_exit=1"></a>');
				});
			</script>

			<div id="elfinder"></div>

		<?php } ?>

	</body>
</html>
