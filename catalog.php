<?php
$page_title = "Маникюр в Приднепровске";
include "includes/head.php";
include "includes/header.php";
?>

<div id="wrapper">
  <div id="wrapper_container">
  <?php
    $dir = "./img/catalog/";
    $folder = scandir($dir);

    for ($i_f = 0; $i_f < count($folder); $i_f++) {
      if (($folder[$i_f] != ".") && ($folder[$i_f] != "..")) {

  			echo "<div class='img_block'><a href='#' class='btn_toggle_cell'><i class='icon icon-plus' ></i>".$folder[$i_f]."</a><div class='catalog_cell'>";
  			$files = scandir($dir.$folder[$i_f]);

  			for ($i = 0; $i < count($files); $i++) {
  		    if (($files[$i] != ".") && ($files[$i] != "..")) {
  		      echo "<div class='catalog_cell_elem'><img class='catalog_img' src='".$dir.$folder[$i_f]."/".$files[$i]."'/></div>";
  		    }
  		  }
        echo "</div></div>";
      }
    }
  ?>
  </div>
</div>

<? include "includes/footer.php"; ?>
